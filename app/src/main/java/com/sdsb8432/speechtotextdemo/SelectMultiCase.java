//package com.fidar.myapplication;
//
//import java.util.ArrayList;
//import java.util.Locale;
//
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.os.Bundle;
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.Context;
//import android.content.Intent;
//import android.speech.RecognizerIntent;
//import android.view.Menu;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.ListView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//
//
//public class MainActivity extends Activity {
//
//    private static final int REQUEST_CODE = 1234;
//    Button Start;
//    TextView Speech;
//    Dialog match_text_dialog;
//    ListView textlist;
//    ArrayList<String> matches_text;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
////معرفی ابزار ها
//        Start = (Button)findViewById(R.id.start_reg);
//        Speech = (TextView)findViewById(R.id.speech);
//
////ایجاد یک لیسنر برای دکمه استارت
//        Start.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(isConnected()){
////استفاده از اینتنت برای فراخوانی و بررسی دسترسی گوشی به اینترنت
//                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, new Locale( "fa", "IR"));
//                    startActivityForResult(intent, REQUEST_CODE);
//                }
//                else{
//                    Toast.makeText(getApplicationContext(), "Plese Connect to Internet",  Toast.LENGTH_LONG).show();
//                }}
//        });
//    }
//    public boolean isConnected()
//    {
////استفاده از کانکت منیجر برای مطمن شدن از دسترسی و عدم دسترسی به اینترنت
//        ConnectivityManager cm = (ConnectivityManager)  getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo net = cm.getActiveNetworkInfo();
//        if (net!=null && net.isAvailable() && net.isConnected()) {
//            return true;
//        } else {
//            return false;
//        }
////برای استفاده از کانکت منیجر باید دسترسی اون رو در منی فست فعال کنیم.
//    }
//
//    //استفاده از اکتیوتی رسولت برای نتیجه گیری از دریافت اطلاعات از google voice و  نمایش ان بروی لیست ویو
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
//            match_text_dialog = new Dialog(MainActivity.this);
//            match_text_dialog.setContentView(R.layout.dialog_matches_frag);
//            match_text_dialog.setTitle("کلمه مدنظر را انتخاب کنید");
//            textlist = (ListView)match_text_dialog.findViewById(R.id.list);
//            matches_text = data
//                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                    android.R.layout.simple_list_item_1, matches_text);
//            textlist.setAdapter(adapter);
//            textlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
////گرفتن اطلاعات از دیالوگ و نمایش تکست کلیک شده
//                public void onItemClick(AdapterView<?> parent, View view,
//                                        int position, long id) {
//                    Speech.setText("Gofteh Shoma:" +matches_text.get(position));
//                    match_text_dialog.hide();
//                }
//            });
////فعال کردن دیالوگ
//            match_text_dialog.show();
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }
//}